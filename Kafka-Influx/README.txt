1. Uruchomienie bazy InfluxDB jako kontener Docker'a:
sudo docker run -d --name="influxdb" --restart on-failure -p 8086:8086 -e INFLUXDB_HTTP_AUTH_ENABLED=true -v influxdb_data:/var/lib/influxdb influxdb -config /etc/influxdb/influxdb.conf
2. Uruchomienie systemu Apache Kafka:
sudo docker run --rm -d -p 2181:2181 -p 3030:3030 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 9092:9092 -e ADV_HOST=[VM_EXTERNAL_IP] landoop/fast-data-dev
3. Stworzenie connectora między Kafką a InfluxDB:
curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d @influxdb.json http://localhost:8083/connectors
4. Stworzenie avro schema w Kafce dla rekordów z sensorów:
curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" --data '{ "schema": "{ \"type\": \"record\", \"name\": \"SensorLog\", \"namespace\": \"com.pwc.sensors\", \"fields\": [ { \"name\": \"id\", \"type\": \"string\" }, { \"name\": \"value\", \"type\": \"double\" }, { \"name\": \"timestamp\", \"type\": \"long\" } ]}" }' http://localhost:8081/subjects/sensors-avro-value/versions
5. Następnie należy stworzyć topic w Kafce o nazwie: "sensors-avro" i stworzyć bazę w InfluxDB o nazwie: "sensors"
6. Ściągnąć Confluent platform ze strony: https://docs.confluent.io/3.0.0/quickstart.html i podążać według instrukcji aby użyć kafka-avro-console-producer 
7. Aby zasymulować sensor należy wykonać komendę:
./sensor.sh sensor_id
