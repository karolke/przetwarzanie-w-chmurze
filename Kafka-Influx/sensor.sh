#!/bin/sh
 
ID=$1
MINRANGE=52
PRECISION=.01
MAXRANGE=68
SERVER=localhost:9092
TOPIC=sensors-avro
SCHEMA='{"type":"record","name":"SensorLog","fields":[{"name":"id","type":"string"},{"name":"value","type":"double"},{"name":"timestamp","type":"long"}]}'
 
timestamp() {
  date +"%s"
}
 
createJSON() {
  echo $( jq -n \
                  --arg id "$ID" \
                  --arg ts $(timestamp) \
		  --arg val $(seq "$MINRANGE" "$PRECISION" "$MAXRANGE" | shuf | head -n1) \
                  '{id: $id, timestamp: $ts|tonumber, value: $val|tonumber}' )
}
 
 
 
while true; do createJSON; sleep 10; done | bin/kafka-avro-console-producer --broker-list "$SERVER" --topic "$TOPIC" --property value.schema="$SCHEMA"
