classdef createPlot < handle
    
    methods
        function draw(obj)
          URL = 'http://40.89.139.34:8086';
USER = 'admin';
PASS = 'admin';
DATABASE = 'sensors';
influxdb = InfluxDB(URL, USER, PASS, DATABASE);

[ok, ping] = influxdb.ping()

% Show the databases
dbs = influxdb.databases()
influxdb.use('sensors');
str = 'SELECT * FROM sensors_temperature';



str1 = 'SELECT * FROM sensors_temperature where id = ''sensor1''  ORDER BY DESC limit 10';
result1 = influxdb.runQuery(str1);
sensors_temperature1 = result1.series('sensors_temperature')
timestamp1 = sensors_temperature1.field('timestamp');
value1 = sensors_temperature1.field('value');

str2 = 'SELECT * FROM sensors_temperature where id = ''sensor2''  ORDER BY DESC limit 10';
result2 = influxdb.runQuery(str2);
sensors_temperature2 = result2.series('sensors_temperature')
timestamp2 = sensors_temperature2.field('timestamp');
value2 = sensors_temperature2.field('value');


str3 = 'SELECT * FROM sensors_temperature where id = ''sensor3''  ORDER BY DESC limit 10';
result3 = influxdb.runQuery(str3);
sensors_temperature3 = result3.series('sensors_temperature')
timestamp3 = sensors_temperature3.field('timestamp');
value3 = sensors_temperature3.field('value');

plot(timestamp1, value1);
title('Real time sensors temperature')

hold on

plot(timestamp2, value2);
plot(timestamp3, value3); 

xlabel('timestamp') 
ylabel('Temp val') 
legend('sensor1','sensor2', 'sensor3')

hold off

        end

    end
    
end